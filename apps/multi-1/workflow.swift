
import io;
import launch;
import stats;
import string;

// Commands and process counts
int procs[] = [3, 4];
cmds = split("./write.sh ./write.sh");

// Command line arguments
string a[][];
a[0] = ["hello.txt", "hello1", "hello2"];
a[1] = ["bye.txt",   "bye1", "bye2", "bye3"];

// Environment variables
string e[][];
e[0] = ["evar1=32",  "evar2=3"];
e[1] = ["evar3=400", "evar4=0"];

int code = @par=sum_integer(procs) launch_multi(procs, cmds, a, e);
printf("exit code: %i", code);
