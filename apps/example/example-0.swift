
import io;
import launch;
import sys;

// This directory: default is "."
this = argv("this", ".");

string a[] = ["abc", "defg"];

program = this/"example.x";

printf("swift: launching: %s", program);
exit_code = @par=3 launch(program, a);
printf("swift: received exit code: %d", exit_code);
if (exit_code != 0)
{
  printf("swift: The launched application did not succeed.");
}
