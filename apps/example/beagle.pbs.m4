changecom(`dnl')#!/bin/bash -e
# We use changecom to change the M4 comment to dnl, not hash

# Created: esyscmd(`date')

# Define a convenience macro
# This simply does environment variable substition when m4 runs
define(`getenv', `esyscmd(printf -- "$`$1'")')

#PBS -A getenv(PROJECT)
#PBS -q getenv(QUEUE)
#PBS -l walltime=getenv(WALLTIME)

#PBS -l nodes=getenv(NODES)
#PBS -l mppwidth=getenv(PROCS)
#PBS -l mppnppn=getenv(PPN)

# Merge stdout/stderr
#PBS -j oe
# Disable mail
#PBS -m n

ARGS="getenv(ARGS)"
NODES=getenv(NODES)
WALLTIME=getenv(WALLTIME)

# Output header
printf "Running: "
date "+%m/%d/%Y %I:%M%p"
echo

PROCS=getenv(PROCS)

# Stream output to file for immediate viewing
echo "JOB OUTPUT is in ${PBS_JOBID}.out"
aprun -n getenv(PROCS) -N getenv(PPN) -cc none -d 1 \
      ./example.x $ARGS
      2>&1 > ${PBS_JOBID}.out

# Local Variables:
# mode: m4
# End:
