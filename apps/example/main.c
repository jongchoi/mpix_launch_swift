
#include <stdio.h>

#include <mpi.h>

int
main(int argc, char* argv[])
{
  int mpi_rank, mpi_size;
  MPI_Init(0, 0);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  printf("rank: %i\n", mpi_rank);
  if (mpi_rank == 0)
  {
    printf("size: %i\n", mpi_size);
    for (int i = 0; i < argc; i++)
      printf("arg:  %i %s\n", i, argv[i]);
  }
  MPI_Finalize();
}
