#!/bin/sh
set -eu

# First (optional) argument is PROCS, default 4
PROCS=4
if [ ${#} -gt 1 ]
then
  PROCS=$1
fi

export THIS=$( cd $( dirname $0 ) ; /bin/pwd )
cd $THIS

if ! [ -x ./example.x ]
then
  echo "Build example.x first!"
  exit 1
fi

mpiexec -n $PROCS ./example.x
