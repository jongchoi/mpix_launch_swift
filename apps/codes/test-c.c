#include <mpi.h>
#include "codes-swift-iface.h"

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);
  int n = 4;
  char** s = (char *[]) { "a", "b", "c", "d" };
  MPI_Comm comm;
  MPI_Comm_dup(MPI_COMM_WORLD,&comm);
  run_codes_mpi_replay(comm, n, s);
  MPI_Finalize();
  return 0;
}
