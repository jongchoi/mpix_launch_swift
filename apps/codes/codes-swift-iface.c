#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <codes/codes-mpi-replay.h>
#include "codes-swift-iface.h"

int run_codes_mpi_replay(MPI_Comm comm, int argc, char** argv)
{

	// Change directory
	char* dir = argv[1];
	chdir(dir);
	printf("Started instance %s\n",dir);

	// Redirecting stdout and stderr temporarily
	int old_stdout, new_stdout, old_stderr, new_stderr;
	fflush(stdout);
	old_stdout = dup(1);
	char filename[128];
	long t = MPI_Wtime();
	sprintf(filename,"stdout-%ld.txt",t);
	new_stdout = open(filename, O_WRONLY | O_CREAT, 0666);
	dup2(new_stdout, 1);
	close(new_stdout);
	
	fflush(stderr);
	old_stderr = dup(2);
	sprintf(filename,"stderr-%ld.txt",t);
	new_stderr = open(filename, O_WRONLY | O_CREAT, 0666);
	dup2(new_stderr, 2);
	close(new_stderr);

	// Calling CODES
	//char** argv2 = argv+2;
	char** argv2 = (char**)malloc(sizeof(char*)*(argc-2));
	int i; for(i=0; i<argc-2;i++) argv2[i] = argv[i+2];
	int argc2 = argc-2;
  int r = modelnet_mpi_replay(comm,&argc2,&argv2);

	free(argv2);

	// Re-establishing stdout and stderr
	fflush(stdout);
	dup2(old_stdout, 1);
	close(old_stdout);
	fflush(stderr);
	dup2(old_stderr, 2);
	close(old_stderr);

  // creating the "completed" file
	FILE* f = fopen("completed","w+");
	fclose(f);

	// Change directory back
	chdir("..");
	printf("Instance %s completed!\n",dir);
	// Free the user communicator
  if(comm != MPI_COMM_SELF 
	&& comm != MPI_COMM_WORLD) {
		MPI_Comm_free(&comm);
	}
	
  return r;
}
