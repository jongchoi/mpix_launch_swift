import io;

@par @dispatch=WORKER (int status) launch(string cmd, string args[])
  "launch" "0.0" "launch_tcl";

@par @dispatch=WORKER (int status) launch_turbine(string cmd, string args[])
  "launch" "0.0" "launch_turbine_tcl";

string a[] = ["abc", "defg"];

/*
MPIX_LAUNCH = "./mpix_launch";

exit_code = @par=2 launch(MPIX_LAUNCH+"/example/child", a);
printf("exit code: %d", exit_code);
*/

exit_code = @par=3 launch_turbine("child.tic", a);
printf("exit code: %d", exit_code);
