#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MPIX_Comm_launch.h"

int launch(MPI_Comm comm, char* cmd, int argc, char** argv) {
	int status = 0;
	char** argvc = (char**)malloc((argc+1)*sizeof(char*));
	int i;
	for(i=0; i<argc; i++) {
		argvc[i] = argv[i];
	}
	argvc[argc] = NULL;
	MPIX_Comm_launch(cmd, argvc, MPI_INFO_NULL, 0, comm, &status);
	free(argvc);
	if(comm != MPI_COMM_SELF) {
		MPI_Comm_free(&comm);
	}
	return status;
}

MPI_Info envs2info(int envc, char** envs) {
  // printf("envs2info: envc=%i\n", envc);
  if (envc == 0)
    return MPI_INFO_NULL;

  MPI_Info info;
  MPI_Info_create(&info);
  char key[16];
  char value[16];
  strcpy(key, "envs");
  sprintf(value, "%i", envc);
  MPI_Info_set(info, key, value);
  for(int i=0; i<envc; i++) {
    sprintf(key, "env%i", i);
    // printf("info set: %s=%s\n", key, envs[i]);
    MPI_Info_set(info,key,envs[i]);
  }
  return info;
}

int launch_envs(MPI_Comm comm, char* cmd,
                int argc, char** argv,
                int envc, char** envs) {
	int status = 0;
	char** argvc = (char**)malloc((argc+1)*sizeof(char*));
	int i;
	for(i=0; i<argc; i++) {
		argvc[i] = argv[i];
	}
	argvc[argc] = NULL;

        MPI_Info info = envs2info(envc, envs);
	MPIX_Comm_launch(cmd, argvc, info, 0, comm, &status);
        if (info != MPI_INFO_NULL) {
          MPI_Info_free(&info);
        }
	free(argvc);
	if(comm != MPI_COMM_SELF) {
		MPI_Comm_free(&comm);
	}
	return status;
}


int launch_turbine(MPI_Comm comm, char* cmd, int argc, char** argv) {
	int status = 0;
	char** argvc = (char**)malloc((argc+1)*sizeof(char*));
	int i;
	for(i=0; i<argc; i++) {
		argvc[i] = argv[i];
	}
	argvc[argc] = NULL;
	MPI_Info info;
	MPI_Info_create(&info);
	MPI_Info_set(info,"launcher","turbine");
	MPIX_Comm_launch(cmd, argvc, info, 0, comm, &status);
	MPI_Info_free(&info);
	free(argvc);
	if(comm != MPI_COMM_SELF) {
		MPI_Comm_free(&comm);
	}
	return status;
}

static int get_color(MPI_Comm comm, int count, int* procs);

int launch_multi(MPI_Comm comm, int count, int* procs,
                 char** cmd,
                 int* argc, char*** argv,
                 int* envc, char*** envs)
{
  int color = get_color(comm, count, procs);
  MPI_Comm subcomm;
  MPI_Comm_split(comm, color, 0, &subcomm);
  int status = 0;
  // printf("launch: color=%i %s\n", color, cmd[color]);
  int result = launch_envs(subcomm, cmd[color],
                           argc[color], argv[color],
                           envc[color], envs[color]);
  MPI_Reduce(&status, &result, 1, MPI_INT, MPI_MAX, 0, comm);
  return status;
}

static void sanity_check(MPI_Comm comm, int count, int* procs);

static int
get_color(MPI_Comm comm, int count, int* procs) {

  sanity_check(comm, count, procs);

  int rank;
  MPI_Comm_rank(comm, &rank);
  int p = 0; // running total procs
  int i;
  for (i = 0; i < count; i++)
  {
    // printf("procs[%i]=%i\n", i, procs[i]);
    p += procs[i];
    if (rank < p)
      return i;
  }
  // Unreachable
  assert(0);
}

static void
sanity_check(MPI_Comm comm, int count, int* procs) {
  int size;
  MPI_Comm_size(comm, &size);
  int total = 0;
  int i;
  for (i = 0; i < count; i++) {
    total += procs[i];
  }
  if (total != size)
  {
    printf("procs total=%i does not equal comm size=%i\n", total, size);
    abort();
  }
}
